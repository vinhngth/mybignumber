package answer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.*;

public class add2num {

    public static String sum(String num1, String num2) {

        StringBuilder sb = new StringBuilder();
        int i = num1.length() - 1, j = num2.length() - 1, carry = 0;
        while (i >= 0 || j >= 0 || carry != 0) {
            int sum = carry;
            if (i >= 0)
                sum += num1.charAt(i--) - '0';

            if (j >= 0)
                sum += num2.charAt(j--) - '0';

            DemoLogger logger = new DemoLogger();
            logger.makeSomeLog(sum % 10);

            sb.append(sum % 10);
            carry = sum / 10;
        }
        return sb.reverse().toString();
    }

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("..\\test\\case.txt");
        Scanner scnr = new Scanner(file);
        while (scnr.hasNextLine()) {
            String n1 = scnr.next();
            String n2 = scnr.next();
            System.out.println(sum(n1, n2));
        }
        scnr.close();
    }
}

class DemoLogger {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void makeSomeLog(int num) {
        LOGGER.log(Level.INFO, num + "");

    }

    public static void main(String[] args) {
    }
}